import React from "react";
import { MarkerType } from "reactflow";

const nodeProps = {
  style: {
    background: "#000",
    color: "#fff",
    border: "2px solid #dc5eb4",
    width: 180,
  },
};

const edgeProps = {
  style: { stroke: "#dc5eb4", strokeWidth: "2" },
};

export const nodes = [
  {
    id: "1",
    type: "input",
    data: {
      label: (
        <>
          Welcome to <strong>React Flow!</strong>
        </>
      ),
    },
    ...nodeProps,
    position: { x: 250, y: 0 },
  },
  {
    id: "2",
    data: {
      label: (
        <>
          This is a <strong>default node</strong>
        </>
      ),
    },
    ...nodeProps,
    position: { x: 100, y: 100 },
  },
  {
    id: "3",
    data: {
      label: (
        <>
          This one has a <strong>custom style</strong>
        </>
      ),
    },
    ...nodeProps,
    position: { x: 400, y: 100 },
  },
  {
    id: "4",
    position: { x: 250, y: 200 },
    data: {
      label: "Another default node",
    },
    ...nodeProps,
  },
  {
    id: "5",
    data: {
      label: "Node id: 5",
    },
    ...nodeProps,
    position: { x: 250, y: 325 },
  },
  {
    id: "6",
    data: {
      label: (
        <>
          An <strong>output node</strong>
        </>
      ),
    },
    ...nodeProps,
    position: { x: 100, y: 480 },
  },
  {
    id: "7",
    ...nodeProps,
    data: { label: "7th node" },
    position: { x: 400, y: 250 },
  },
  {
    id: "8",
    ...nodeProps,
    data: { label: "8th node" },
    position: { x: 0, y: 350 },
  },
  {
    id: "9",
    ...nodeProps,
    data: { label: "9th node" },
    position: { x: -100, y: 150 },
  },
  {
    id: "10",
    ...nodeProps,
    data: { label: "10th node" },
    position: { x: 0, y: 0 },
  },
  {
    id: "11",
    ...nodeProps,
    data: { label: "11th node" },
    position: { x: 50, y: 250 },
  },
];

export const edges = [
  {
    id: "e1-2",
    source: "1",
    target: "2",
    ...edgeProps,
    type: "smoothstep",
  },
  {
    id: "e1-3",
    source: "1",
    target: "3",
    type: "smoothstep",
    ...edgeProps,
  },
  {
    id: "e3-4",
    source: "3",
    target: "4",
    animated: true,
    type: "smoothstep",
    // label: "animated edge",
    ...edgeProps,
  },
  {
    id: "e4-5",
    source: "4",
    target: "5",
    type: "smoothstep",
    // label: "edge with arrow head",
    ...edgeProps,
    markerEnd: {
      type: MarkerType.ArrowClosed,
    },
  },
  {
    id: "e5-6",
    source: "5",
    target: "6",
    type: "smoothstep",
    // label: "smooth step edge",
    ...edgeProps,
  },
  {
    id: "e6-7",
    source: "6",
    target: "7",
    type: "step",
    ...edgeProps,
    // label: "a step edge",
    // labelStyle: { fill: "#f6ab6c", fontWeight: 700 }
  },
  {
    id: "e7-8",
    source: "7",
    target: "8",
    type: "step",
    ...edgeProps,
    // label: "a step edge",
    // labelStyle: { fill: "#f6ab6c", fontWeight: 700 }
  },
  {
    id: "e8-9",
    source: "8",
    target: "9",
    type: "step",
    ...edgeProps,
    // label: "a step edge",
    // labelStyle: { fill: "#f6ab6c", fontWeight: 700 }
  },
  {
    id: "e9-10",
    source: "9",
    target: "10",
    type: "step",
    ...edgeProps,
    // label: "a step edge",
    animated: true,
    // labelStyle: { fill: "#f6ab6c", fontWeight: 700 }
  },
  {
    id: "e10-11",
    source: "10",
    target: "11",
    type: "step",
    ...edgeProps,
    
    // label: "a step edge",
    // labelStyle: { fill: "#f6ab6c", fontWeight: 700 }
  },
//   {
//     id: "e11-12",
//     source: "9",
//     target: "11",
//     type: "step",
//     ...edgeProps,
    
//     // label: "a step edge",
//     // animated: true,
//     // labelStyle: { fill: "#f6ab6c", fontWeight: 700 }
//   },
];
