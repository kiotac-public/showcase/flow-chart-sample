import logo from "./logo.svg";
import "./App.css";
import Flow from "./components/Flow";

function App() {
  return <Flow />;
}

export default App;
